FROM php:8.1-fpm-alpine

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN apk update

RUN apk add \
        curl \
        git \
        imagemagick-dev \
        imap-dev \
        libpng-dev \
        libxml2-dev \
        libzip-dev \
        openssh-client \
        php81-curl \
        php81-gd \
        php81-intl \
        php81-mysqli \
        php81-pecl-imagick \
        php81-redis \
        php81-zlib

RUN apk add \
        icu-libs \
        icu \
        icu-dev

# imagick
RUN apk add --update --no-cache autoconf g++ imagemagick-dev libtool make pcre-dev \
    && pecl install imagick \
    && docker-php-ext-enable imagick \
    && apk del autoconf g++ libtool make pcre-dev

RUN apk update \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* || true

# php extensions
RUN chmod +x /usr/local/bin/install-php-extensions && \
    install-php-extensions \
        amqp \
        bcmath \
        exif \
        gd \
        imap \
        intl \
        mysqli \
        pdo \
        pdo_mysql \
        redis \
        soap \
        sockets \
        xdebug \
        xml \
        zip

# composer
ENV COMPOSER_ALLOW_SUPERUSER=1
RUN set -eux
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
